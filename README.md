[![pipeline status](https://gitlab.com/autonomic-roles/ssh/badges/master/pipeline.svg)](https://gitlab.com/autonomic-roles/ssh/commits/master)

# ssh

A role to arrange basic guarantees about SSH.

# Hack On It

```bash
$ pip install -r requirements.txt
```

```bash
$ molecule test
```

# Using the Role

```yaml
  - hosts: server
    roles:
      - ssh
```
