def test_ssh_agent_forwarding_configured(host):
    sudoers = host.file('/etc/sudoers')
    assert sudoers.contains('ssh_auth_sock')
